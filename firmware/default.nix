{ stdenvNoCC, fetchFromGitHub, lib }: stdenvNoCC.mkDerivation {
  name = "orangepi-xunlong-firmware";
  src = fetchFromGitHub {
    owner = "orangepi-xunlong";
    repo = "firmware";
    rev = "75747c7034b1136b4674269e248b69bf1a5e4039";
    hash = "sha256-FrpMzju2drVToEWk/Oz9yirERfAhAP8PaCcVCRmm0WM=";
  };

  phases = [ "unpackPhase" "installPhase" ];
  installPhase = ''
    mkdir -p $out/lib
    cp -r $src $out/lib/firmware
  '';

  meta = {
    platforms = [ "aarch64-linux" ];
    description = "Orange Pi specific firmware";
    homepage = "https://github.com/orangepi-xunlong/firmware";
    license = lib.licenses.unfreeRedistributable;
  };
}
