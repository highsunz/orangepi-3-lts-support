#!/usr/bin/env python3

from concurrent.futures import ThreadPoolExecutor
import logging
from pathlib import Path
import sys
from typing import Dict, List, Optional, Tuple, Union
import urllib.error
import urllib.request
import re


def setup_logging():
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s'
    )

def download_file(url: str, headers: Optional[dict] = None) -> Optional[bytes]:
    """Download a file from given URL."""
    if headers is None:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'
        }

    try:
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req, timeout=30) as response:
            return response.read()
    except urllib.error.URLError as e:
        logging.error(f"Failed to download from {url}: {str(e)}")
    except urllib.error.HTTPError as e:
        logging.error(f"HTTP Error {e.code} while downloading from {url}: {str(e)}")
    except TimeoutError:
        logging.error(f"Timeout while downloading from {url}")
    except Exception as e:
        logging.error(f"Unexpected error while downloading from {url}: {str(e)}")

    return None

def parse_kernel_config(content: str) -> Dict[str, Union[str, bool]]:
    """Parse kernel config file content and return config options."""
    config_dict = {}
    # Keys to skip - version related and compiler/tool related
    skip_patterns = set(map(re.compile, [
        # Kernel version related
        "VERSION", "LOCALVERSION", "LOCALVERSION_AUTO",
        # Compiler and toolchain versions
        "CC_VERSION_TEXT", "GCC_VERSION", "CLANG_VERSION",
        "LD_VERSION", "LLD_VERSION", "AS_VERSION",
        "PAHOLE_VERSION",  # BTF generation tool version
        "INITRAMFS_SOURCE", "INITRAMFS_PRESERVE_MTIME", "BLK_DEV_INITRD",  # ../usr/gen_initramfs.sh: Cannot open ''
        # # Certificate related configs that might cause issues
        # "SYSTEM_TRUSTED_KEYS", "SYSTEM_REVOCATION_KEYS",
        # "SYSTEM_TRUSTED_KEYRING", "SECONDARY_TRUSTED_KEYRING",
        # "SYSTEM_BLACKLIST_KEYRING", "SYSTEM_BLACKLIST_HASH_LIST",
        # Skip all crypto configs
        "CRYPTO",
        # Skip debug-related configs
        "DEBUG",
        "^TASKS_TRACE_RCU$",
        "^SYSCTL_EXCEPTION_TRACE$",
        "^RCU_TRACE$",
        "^TRACE_CLOCK$",
        "^TRACE_IRQFLAGS_SUPPORT$",
        "^TRACE_IRQFLAGS_NMI_SUPPORT$",
        "^HAVE_FUNCTION_TRACER$",
        "^HAVE_FUNCTION_GRAPH_TRACER$",
        "^HAVE_DYNAMIC_FTRACE$",
        "^HAVE_DYNAMIC_FTRACE_WITH_ARGS$",
        "^HAVE_FTRACE_MCOUNT_RECORD$",
        "^HAVE_SYSCALL_TRACEPOINTS$",
        # Keep architecture specific version configs
        # "ARCH_WANT_IPC_PARSE_VERSION" is kept as it's a feature flag, not a version number
    ]))

    for line in content.split('\n'):
        line = line.strip()
        if not line or line.startswith('#'):
            continue
        if '=' in line:
            key, value = line.split('=', 1)
            if key.startswith('CONFIG_'):
                key = key[7:]  # Remove CONFIG_ prefix
                if any([pattern.search(key) for pattern in skip_patterns]):
                    continue
                if value.strip('"') == "":
                    continue
                config_dict[key] = value
    return config_dict

def convert_to_nix_value(value: str) -> str:
    """Convert kernel config value to Nix kernel lib value."""
    value = value.strip('"')
    if value == 'y':
        return 'yes'
    elif value == 'n':
        return 'no'
    elif value == 'm':
        return 'module'
    else:
        return f'freeform "{value}"'

def write_structured_config_nix(base_dir: Path, config_dict: Dict[str, Union[str, bool]]):
    """Write structured-config.nix file."""
    content = [
        "# Auto generated file",
        "",
        "{ lib }:",
        "",
        "with lib.kernel;",
        "",
        "{"
    ]

    # Fixes for compilation
    fixes = {
        # nixos kernel config only allows ARCH_MMAP_RND_BITS to be 18
        "ARCH_MMAP_RND_BITS": "18",
        "ARCH_MMAP_RND_BITS_MIN": "18",
        "ARCH_MMAP_RND_BITS_MAX": "24",
        # armbian's patches missed this and we don't need it
        "CLK_IMX8MP": "n",
        # Fix for GPADC driver conflict
        "MFD_SUN4I_GPADC": "m",
        "TOUCHSCREEN_SUN4I": "n",
        # nixos seems to enable those so we don't disable them here.  REF: `zcat /proc/config.gz | less`
        # # ensure we're getting a release build without debug overhead
        # "DEBUG_KERNEL": "n",
        # "FTRACE": "n",
        # "KPROBES": "n",
    }

    config_dict |= fixes

    # Add config entries
    for key, value in config_dict.items():
        nix_value = convert_to_nix_value(value)
        content.append(f"  \"{key}\" = lib.mkOverride 75 ({nix_value});")

    content.append("}")

    try:
        (base_dir / 'structured-config.nix').write_text('\n'.join(content))
        logging.info("Successfully wrote structured-config.nix")
    except Exception as e:
        logging.error(f"Failed to write structured-config.nix: {str(e)}")

def create_directory_structure(base_dir: Path):
    """Create the necessary directory structure for patches."""
    directories = ['patches.armbian', 'patches.fixes', 'patches.megous']
    for directory in directories:
        (base_dir / directory).mkdir(parents=True, exist_ok=True)

def parse_series_conf(file_content: str) -> List[str]:
    """Parse the series.conf file and return a list of valid patch paths."""
    patches = []
    for line in file_content.split('\n'):
        line = line.strip()
        if not line or line.startswith('#') or line.startswith('-'):
            continue
        patches.append(line)
    return patches

def download_single_patch(args: Tuple[str, str, Path]) -> Tuple[str, bool]:
    """Download a single patch file. Returns (patch_path, success)."""
    patch_path, base_url, base_dir = args
    local_path = base_dir / patch_path
    local_path.parent.mkdir(parents=True, exist_ok=True)
    url = f"{base_url}/{patch_path}"

    content = download_file(url)
    if content is None:
        return patch_path, False

    try:
        local_path.write_bytes(content)
        logging.info(f"Successfully downloaded: {patch_path}")
        return patch_path, True
    except Exception as e:
        logging.error(f"Failed to save {patch_path}: {str(e)}")
        return patch_path, False

def download_patches_parallel(patches: List[str], base_url: str, base_dir: Path, max_workers: int = 10) -> List[str]:
    """Download patches in parallel using ThreadPoolExecutor."""
    successful_patches = []
    args_list = [(patch, base_url, base_dir) for patch in patches]

    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        futures = list(executor.map(download_single_patch, args_list))

        for patch_path, success in futures:
            if success:
                successful_patches.append(patch_path)

    return successful_patches

def write_patches_nix(base_dir: Path, successful_patches: List[str]):
    """Write patches.nix file containing a Nix list of all downloaded patches."""
    patches_nix_content = "[\n"

    for patch in successful_patches:
        relative_path = f"  ./{patch}"
        patches_nix_content += f"{relative_path}\n"

    patches_nix_content += "]\n"

    try:
        (base_dir / 'patches.nix').write_text(patches_nix_content)
        logging.info("Successfully wrote patches.nix")
    except Exception as e:
        logging.error(f"Failed to write patches.nix: {str(e)}")

def main():
    if len(sys.argv) not in [2, 3]:
        print("Usage: python download_patches.py <version> [config-name]")
        print("Example: python download_patches.py sunxi-6.6 linux-sunxi64-current.config")
        sys.exit(1)

    version = sys.argv[1]
    config_name = sys.argv[2] if len(sys.argv) > 2 else None

    setup_logging()
    base_dir = Path.cwd()
    base_url = f"https://raw.githubusercontent.com/armbian/build/main/patch/kernel/archive/{version}"

    # Download and process kernel config if name provided
    if config_name:
        config_url = f"https://raw.githubusercontent.com/armbian/build/main/config/kernel/{config_name}"
        logging.info("Downloading kernel config...")
        config_content = download_file(config_url)
        if config_content is not None:
            config_text = config_content.decode('utf-8')
            config_dict = parse_kernel_config(config_text)
            write_structured_config_nix(base_dir, config_dict)
        else:
            logging.error("Failed to download kernel config")

    # Download series.conf
    logging.info("Downloading series.conf...")
    series_content = download_file(f"{base_url}/series.conf")

    if series_content is None:
        logging.error("Failed to download series.conf")
        sys.exit(1)

    try:
        series_text = series_content.decode('utf-8')
    except UnicodeDecodeError as e:
        logging.error(f"Failed to decode series.conf: {str(e)}")
        sys.exit(1)

    create_directory_structure(base_dir)
    patches = parse_series_conf(series_text)
    total_patches = len(patches)

    logging.info(f"Found {total_patches} patches to download")

    successful_patches = download_patches_parallel(patches, base_url, base_dir)
    success_count = len(successful_patches)

    logging.info(f"Download complete. Successfully downloaded {success_count} out of {total_patches} patches.")

    write_patches_nix(base_dir, successful_patches)

if __name__ == "__main__":
    main()
