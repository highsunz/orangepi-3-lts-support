{ pkgs, lib, base
# NOTE: add `...` below because pkgs.callPackage also supplies unused argument(s) like
# `kernelPatches`.
, ...
}:

let
  kernel = base.override {
    kernelPatches = (with pkgs.kernelPatches; [
      bridge_stp_helper
      request_key_helper
    ]) ++ [{
      name = "armbian-build-sunxi";
      # generate ./patches.nix and ./structured-config.nix with:
      #
      #   python3 download.py sunxi-6.6 linux-sunxi-current.config
      patch = (import ./driver-patches) ++ (import ./patches.nix);
      extraStructuredConfig = import ./structured-config.nix { inherit lib; };
    }];
  };
in

kernel.overrideDerivation (drv: {
  nativeBuildInputs = drv.nativeBuildInputs or [] ++ [ pkgs.ubootTools ];
})
