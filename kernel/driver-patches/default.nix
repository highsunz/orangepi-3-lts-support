let
  uwe5622-driver = [
    # REF: <https://github.com/armbian/build/blob/06c37e510d168f848a8e769229e801dbdd7e1a1d/lib/functions/compilation/patch/drivers_network.sh#L455-L529>
    # ask Claude to read the above function and predict the used patches when we are targeting a
    # specific version (6.6 in this case), and output them in the original order.
    ./wireless-uwe5622/uwe5622-allwinner-v6.3.patch
    ./wireless-uwe5622/uwe5622-allwinner-bugfix-v6.3.patch
    ./wireless-uwe5622/uwe5622-allwinner-v6.3-compilation-fix.patch
    ./wireless-uwe5622/uwe5622-v6.4-post.patch
    ./wireless-uwe5622/uwe5622-warnings.patch
    ./wireless-uwe5622/uwe5622-park-link-v6.1-post.patch
    ./wireless-uwe5622/uwe5622-v6.1.patch
    ./wireless-uwe5622/uwe5622-v6.6-fix-tty-sdio.patch
    ./wireless-uwe5622/uwe5622-fix-setting-mac-address-for-netdev.patch
    ./wireless-uwe5622/wireless-uwe5622-Fix-compilation-with-6.7-kernel.patch
    ./wireless-uwe5622/wireless-uwe5622-reduce-system-load.patch
  ];
  uwe5622-fixes = [
    ./add-missing-wlan-vendor-config-option.patch
    ./fix-uwe5622-unisocwcn-include-path.patch
    ./fix-wcn-chmod-path-for-nixos.patch
  ];
in
  uwe5622-driver ++ uwe5622-fixes
