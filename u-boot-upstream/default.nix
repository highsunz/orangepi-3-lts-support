{ stdenvNoCC, lib }: stdenvNoCC.mkDerivation rec {
  pname = "uboot";
  version = "Orangepi3-lts_3.0.8_debian_bullseye_server_linux5.16.17";
  src = ./uboot.bin;

  buildCommand = ''
    mkdir -p $out/nix-support
    cp $src $out/uboot.bin
    echo "file binary-dist" $out/uboot.bin >$out/nix-support/hydra-build-products
  '';

  meta = {
    homepage = "http://www.orangepi.org/html/hardWare/computerAndMicrocontrollers/service-and-support/Orange-pi-3-LTS.html";
    description = ''
      U-Boot binary extracted from OrangePi's officially released Debian image (filename is
      `${version}`).

      U-Boot package for OrangePi3-LTS typically has size around 600-700KB, and the downloaded
      Debian image's first filesystem starts at sector 8192 (with sector size 512 bytes, amounting
      to 4194304 = 0x00040000 byte offset), while OrangePi3-LTS expects to find the u-boot at 0x2000
      byte offset, therefore extracting a 1000KiB blob from the image should be enough.

      To extract the U-Boot blob, read 1000*1024 bytes beginning at the 1024*8-th byte:

        dd if=${version} of=uboot.bin bs=1KiB count=1000 skip=8
    '';
    license = lib.licenses.unfreeRedistributable;
  };
}
