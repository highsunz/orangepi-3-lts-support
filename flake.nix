{
  description = "Kernel and U-Boot for Orange Pi 3 LTS";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/release-24.11";

  outputs = { self, nixpkgs }: let
    system = "aarch64-linux";
    pkgs = import nixpkgs {
      inherit system;
      config.allowUnfree = true;  # the firmware package has unfree license
    };
  in {
    packages.${system} = {
      # atf
      armTrustedFirmwareAllwinnerH6 = pkgs.callPackage ./atf {};

      # u-boot
      # To install/upgrade u-boot on the board, use:
      #     dd if=result/u-boot-sunxi-with-spl.bin of=/dev/sdX bs=1k seek=8
      # Where /dev/sdX should be the actual plugged SD card (e.g. /dev/sdc).
      #
      # Or, write to the image before burning to the disk:
      #     dd if=result/u-boot-sunxi-with-spl.bin of=img bs=1k seek=8 conv=notrunc
      # Note the conv=notrunc parameter, otherwise the image will be truncated by dd.
      #
      # Example: <https://gitlab.com/highsunz/flames/-/blob/63c777e001a344c7d01dfe8a9733e6aa2d91987e/nixos/opi/configuration.nix#L40>
      ubootOrangePi3LtsUpstream = pkgs.callPackage ./u-boot-upstream {};
      ubootOrangePi3LtsArmbian = pkgs.callPackage ./u-boot-armbian {};

      # kernel
      # Adds device tree and Wi-Fi driver.
      linux = pkgs.callPackage ./kernel { base = pkgs.linux; };

      # firmware
      # Wi-Fi support needs the `sprdwl_ng` kernel module and putting the firmware under path
      # /lib/firmware.
      firmware = pkgs.callPackage ./firmware {};
    };

    hydraJobs = self.packages.${system};
  };
}
