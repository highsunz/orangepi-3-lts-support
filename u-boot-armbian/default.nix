{ buildUBoot
, armTrustedFirmwareAllwinnerH6
}:

buildUBoot {
  extraPatches = [
    # Adapted from: <https://github.com/armbian/build/pull/5758>
    # <https://github.com/armbian/build/blob/8f21f5e8a51a9684168be48a203befaffafb0f1c/patch/u-boot/u-boot-sunxi/Allwinner-Fix-incorrect-ram-size-detection-for-H6-boards.patch>
    ./patches/Allwinner-Fix-incorrect-ram-size-detection-for-H6-boards.patch
    # <https://github.com/armbian/build/blob/889183b78c24ae5435674d8bbf3b2e7b27562e67/patch/u-boot/u-boot-sunxi/board_orangepi3-lts/0001-add-orange-pi-3-lts-support.patch>
    ./patches/0001-add-orange-pi-3-lts-support.patch
  ];

  defconfig = "orangepi_3_lts_defconfig";
  extraMeta = {
    description = ''
      OrangePi3-LTS expects to find the u-boot at 0x2000 byte offset (check this by downloading an
      official image from <http://www.orangepi.org/html/hardWare/computerAndMicrocontrollers/service-and-support/Orange-pi-3-LTS.html>
      and use hexyl to find the starting bytes of u-boot binary starts at 0x2000).
      Also, see <https://nixos.wiki/wiki/NixOS_on_ARM/Orange_Pi_Zero2_H616#Periphery>.

      To place the u-boot at 0x2000 byte offset of the image to be flashed on to the bootable disk
      for OrangePi3-LTS:

        dd if=u-boot-sunxi-with-spl.bin of=./path/to/image.raw bs=1KiB seek=8 conv=notrunc
    '';
    platforms = [ "aarch64-linux" ];
  };
  BL31 = "${armTrustedFirmwareAllwinnerH6}/bl31.bin";
  SCP = "/dev/null";
  extraConfig = ''
    CONFIG_CMD_BTRFS=y
    CONFIG_FS_BTRFS=y
  '';
  filesToInstall = [ "u-boot-sunxi-with-spl.bin" ];
}
